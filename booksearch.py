#!/usr/bin/python
# *********************************************************************
# * Copyright (C) 2017 Jacopo Nespolo <j.nespolo@gmail.com>           *
# *                                                                   *
# * booksearch is free software, released according to the terms of   *
# * the GNU General Public License, version 3.                        *
# * For the license terms see the file LICENCE, distributed along     *
# * with this software or at <http://www.gnu.org/licenses/gpl.txt>.   *
# *********************************************************************

from urllib.request import urlopen
import json

class Book:
    def __init__(self, isbn):
        self.isbn = isbn
        self._prefix =  self.isbn[:5]
        self.title = None
        self.author = None
        self.city = None 
        self.publisher = None
        self.year = None
        self.cover = None
        self.query_by_isbn()

    def query_by_isbn(self):
        # query the Italian national library catalog
        if self._prefix == "97888" or self._prefix == "97912": # italian book
            query_url = "http://opac.sbn.it/opacmobilegw/search.json?isbn={}"
            res = urlopen(query_url.format(isbn)).read().decode('utf-8')
            res = json.loads(res)
            res = res['briefRecords'][0]
            self.title = res['titolo'].split(' /')[0]
            self.author = res['autorePrincipale']
            self.city, rest = res['pubblicazione'].split(' : ')
            self.publisher, self.year = rest.split(', ')
            self.cover = res['copertina']

    def __repr__(self):
        out =  "Book:\n"
        out += "    Author    = {}\n".format(self.author)
        out += "    Title     = {}\n".format(self.title)
        out += "    ISBN      = {}\n".format(self.isbn)
        out += "    Publisher = {}, {}, {}\n".format(self.publisher,
                                                     self.city,
                                                     self.year)
        return out


if __name__ == "__main__":
    from sys import exit, argv, stderr
    try:
        isbn = argv[1]
    except IndexError:
        try:
            from PIL import Image
            import zbarlight
            import cv2

            # read barcode from webcam. To make sure that it is read correctly,
            # wait until the same code is read twice.
            cap = cv2.VideoCapture(0)
            if not cap.isOpened():
                raise ValueError("Cannot reach camera")

            cv2.namedWindow('frame', cv2.WINDOW_AUTOSIZE)
            oldcode = None
            for i in range(1000):
                is_successful, frame = cap.read()
                cv2.imshow('frame', frame)
                cv2.waitKey(100)
                code = zbarlight.scan_codes('ean13', Image.fromarray(frame))
                print(i, code)
                if code is not None:
                    if oldcode == code[0]:
                        isbn = code[0].decode('utf-8')
                        break
                    else:
                        oldcode = code[0]
            cap.release()
            cv2.destroyAllWindows()
        except ImportError as e:
            stderr.write("USAGE: {} [isbn]\n\n".format(argv[0]))
            stderr.write(("Fix the following import error to enable "
                "barcode scaninng"))
            stderr.write(str(e) + "\n")
            exit(1)
        except ValueError as e:
            stderr.write("USAGE: {} [isbn]\n\n".format(argv[0]))
            stderr.write("The webcam is unreachable.\n")
            exit(1)
    b = Book(isbn)
    print(b)

    exit(0)

